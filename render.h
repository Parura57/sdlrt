#ifndef __render_H
#define __render_H

#include<SDL2/SDL.h>
#include<string>
#include<unordered_map>

#include"objects.h"

class Renderer {
private:
    Camera* camera;

    SDL_Window* w;
    SDL_Surface *windowSurface;

public:
    void Init(Camera* cam);
    void Render(std::unordered_map<std::string, Object3D*> world);
};

#endif
