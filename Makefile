CXX=$(shell which g++)
INCLUDE=$(shell pkg-config --cflags --libs sdl2)
CXXFLAGS=-O2

TARGETS=render.o math.o objects.o

all: clean ${TARGETS}
	${CXX} main.cpp -o raytracer ${INCLUDE} ${CXXFLAGS} ${TARGETS}

%.o : %.cpp
	${CXX} -c $< -o $@ ${INCLUDE} ${CXXFLAGS}

clean:
	rm -f raytracer *.o

devclean:
	rm -f $(forcebuild)

dev: devclean ${TARGETS}
	${CXX} main.cpp -o raytracer -g3 ${INCLUDE} ${CXXFLAGS} ${TARGETS}

.PHONY: all clean devclean dev run
