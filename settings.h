#ifndef __SETTINGS_H
#define __SETTINGS_H

#include"objects.h"

const int screenW = 300;
const int screenH = 300;
const int raysPerPixel = 1;
const int bounces = 2;

#endif
