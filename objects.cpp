#include"objects.h"

#include<limits>
#include<cstdlib>

// Object3D
Object3D::Object3D(Vector3 position, Vector3 rotation, Vector3 scale, Material material)
    : position(position), rotation(rotation), scale(scale), material(material) {}

Vector3 Object3D::GetPosition() { return position; }
Vector3 Object3D::GetRotation() { return rotation; }
Vector3 Object3D::GetScale() { return scale; }
Material Object3D::GetMaterial() { return material; }

void Object3D::SetPosition(Vector3 p) { position = p; }
void Object3D::SetRotation(Vector3 r) { rotation = r; }
void Object3D::SetScale(Vector3 s) { scale = s; }


// Sphere
Sphere::Sphere(Vector3 position, Vector3 rotation, Vector3 scale, Material material)
    : Object3D::Object3D{position, rotation, scale, material} {}


// Camera
Camera::Camera(Vector3 position, Vector3 rotation, Vector3 scale, double fov)
    : Object3D::Object3D{position, rotation, scale, Material{Vector3{0,0,0}, 0, Vector3{0,0,0}}}, fov(fov) {}

double Camera::GetFov() { return fov; }


// Ray
Ray::Ray(Vector3 origin, Vector3 direction) : origin(origin), direction(direction) {}

// Pack the constants into a struct for cleaner function calls
struct r_RaycastArgs_Sphere {
    double sphereR;
    Vector3 sphereC;
    Vector3 rayO;
    Vector3 rayD;
};

double r_IntersectF_Sphere(double n, r_RaycastArgs_Sphere args);
double r_dIntersectF_Sphere(double n, r_RaycastArgs_Sphere args);
double r_Newton_Sphere(int iterations, r_RaycastArgs_Sphere args);
Vector3 r_BouncedRayDirection(Vector3 incident, Vector3 normal, double specular);
Vector3 r_ReflectVector(Vector3 vec, Vector3 reflector);

const Vector3 SkyboxColor = {50, 50, 50};

HitInfo Ray::Cast(int bounces, std::unordered_map<std::string, Object3D*> world) {
    // For now let's assume we can only intersect spheres (cubes shouldn't be hard later)
    // (xo + k*xd - xc)^2 + (yo + k*yd - yc)^2 + (zo + k*zd - zc)^2 - r^2 = 0
    // Solve for k, then distance to intersection = k*|d| and pos of intersection = k*d
    // f = k^2*xd^2 + k(2*xo*xd + 2*xd*xc) - 2*xo*xc + xo^2 + xc^2 + ... - r^2 = 0
    // df = 2k*xd^2 + (2*xo*xd + 2*xd*xc)

    // Store the last hit to keep only the closest one
    HitInfo bestHit {Vector3 {0, 0, 0}, NULL};
    double bestHitDistance = std::numeric_limits<double>::max(); 

    if (bounces < 0) {
        // Skybox
        bestHit.color = SkyboxColor;
        return bestHit;
    }

    //printf("raycasting from %f %f %f towards %f %f %f\n", origin.x, origin.y, origin.z, direction.x, direction.y, direction.z);

    // Check for hits
    for (auto const entry : world) {
        // Disgusting way to check the type of the current object
        if (static_cast<Sphere*>(entry.second)) {
            double k = r_Newton_Sphere(100, r_RaycastArgs_Sphere{entry.second->GetScale().x, entry.second->GetPosition(), origin, direction});
            // Huh
            if (k >= 0) continue;

            double distance = k*V_Norm(direction);
            if (distance < bestHitDistance) {
                bestHit.coordinates = k*direction;
                bestHit.object = entry.second;
                bestHitDistance = distance;
            }
        }
    }

    // If the ray ends up not hitting anything
    if (bestHit.object == NULL) {
        // Skybox
        bestHit.color = SkyboxColor;
        return bestHit;
    }

    // Send out new ray in a random direction from position of collision
    Vector3 normalUnnormed = V_GetVector(bestHit.object->GetPosition(), bestHit.coordinates);
    Vector3 normal = (1/V_Norm(normalUnnormed))*normalUnnormed;
    Ray newRay(bestHit.coordinates, r_BouncedRayDirection(direction, normalUnnormed, bestHit.object->GetMaterial().specular));
    HitInfo NextHit = newRay.Cast(bounces-1, world);

    // Handle colors
    bestHit.color = NextHit.color;
    bestHit.color.x *= bestHit.object->GetMaterial().color.x / 256;
    bestHit.color.y *= bestHit.object->GetMaterial().color.y / 256;
    bestHit.color.z *= bestHit.object->GetMaterial().color.z / 256;
    bestHit.color = bestHit.color + bestHit.object->GetMaterial().emission;
    // TODO Bad math
    if (bestHit.object->GetMaterial().emission != Vector3 {0, 0, 0}) bestHit.color = 0.5 * bestHit.color;
    //if (NextHit.object != NULL) printf("bounced ray hit at x %.1f with color %.1f %.1f %.1f, returning %.1f %.1f %.1f\n", NextHit.object->GetPosition().x, NextHit.color.x, NextHit.color.y, NextHit.color.z, bestHit.color.x, bestHit.color.y, bestHit.color.z);
    return bestHit;
}


double r_IntersectF_Sphere(double n, r_RaycastArgs_Sphere args) {
    // Unwrap arguments, wasteful but slightly more readable
    double sphereR = args.sphereR;
    Vector3 sphereC = args.sphereC;
    Vector3 rayO = args.rayO;
    Vector3 rayD = args.rayD;

    return n*n*(rayD.x*rayD.x) + n*(2*rayO.x*rayD.x + 2*rayD.x*args.sphereC.x) - 2*rayO.x*args.sphereC.x + rayO.x*rayO.x + args.sphereC.x*args.sphereC.x
    + n*n*(rayD.y*rayD.y) + n*(2*rayO.y*rayD.y + 2*rayD.y*sphereC.y) - 2*rayO.y*sphereC.y + rayO.y*rayO.y + sphereC.y*sphereC.y
    + n*n*(rayD.z*rayD.z) + n*(2*rayO.z*rayD.z + 2*rayD.z*sphereC.z) - 2*rayO.z*sphereC.z + rayO.z*rayO.z + sphereC.z*sphereC.z
    - sphereR*sphereR;
}

double r_dIntersectF_Sphere(double n, r_RaycastArgs_Sphere args) {
    double sphereR = args.sphereR;
    Vector3 sphereC = args.sphereC;
    Vector3 rayO = args.rayO;
    Vector3 rayD = args.rayD;

    return 2*n*(rayD.x*rayD.x) + (2*rayO.x*rayD.x + 2*rayD.x*sphereC.x)
    + 2*n*(rayD.y*rayD.y) + (2*rayO.y*rayD.y + 2*rayD.y*sphereC.y)
    + 2*n*(rayD.z*rayD.z) + (2*rayO.z*rayD.z + 2*rayD.z*sphereC.z);
}

// Should be redone for other equations with function pointers or something
double r_Newton_Sphere(int iterations, r_RaycastArgs_Sphere args) {
    double result = 0; 
    for (int i = 0; i < iterations; i++) {
        result = result - r_IntersectF_Sphere(result, args) / r_dIntersectF_Sphere(result, args);
    }

    // Sanity check, if the approximation is too inaccurate then there is no intersection, may break for objects too close or too far
    if (r_IntersectF_Sphere(result, args) >= 0.0001) result = 0;

    return result;
}

Vector3 r_BouncedRayDirection(Vector3 incident, Vector3 normal, double specular) {
    // Diffuse relection
     Vector3 diff_out {(double) std::rand() / (RAND_MAX), (double) std::rand() / (RAND_MAX), (double) std::rand() / (RAND_MAX)};
    if (diff_out * normal < 0) diff_out = -diff_out;
    // Specular reflection
    Vector3 spec_out = r_ReflectVector(incident, normal);

    // Blend
    if (specular > 1) { printf("Specular value overflow, assuming 1\n"); return spec_out; }
    if (specular < 0) { printf("Specular value negative, assuming 0\n"); return diff_out; }
    Vector3 out = specular * spec_out + (1-specular) * diff_out;
    return out;
    return diff_out;
}

Vector3 r_ReflectVector(Vector3 vec, Vector3 reflector) {
    double norm = V_Norm(vec);

    Vector3 temp = V_XProduct(reflector, vec);
    Vector3 out = V_XProduct(reflector, temp);
    out = norm * out;
    return out;
}

