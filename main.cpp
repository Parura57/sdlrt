#include<string>
#include<unordered_map>

#include"math.h"
#include"objects.h"
#include"render.h"
#include"settings.h"

bool running = true;

void EventHandler (SDL_Event e) {
    switch(e.type) {
    case SDL_QUIT:
        running = false;
        break;

    case SDL_KEYDOWN:
        if (e.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
            running = false;
            break;
        }
        break;
    }
}

int main() {
    // Create and populate the world
    std::unordered_map<std::string, Object3D*> world;
    world["camera"] = new Camera(Vector3{0, 0, 0}, Vector3{0, 0, 0}, Vector3{0, 0, 0}, 90);
    world["sphere"] = new Sphere(Vector3{4, 1, 3}, Vector3{0, 0, 0}, Vector3{1, 0, 0}, Material{Vector3{200, 200, 200}, 0, Vector3{0, 0, 0}});
    world["sphere2"] = new Sphere(Vector3{5, 1.5, 2}, Vector3{0, 0, 0}, Vector3{0.5, 0, 0}, Material{Vector3{0, 200, 255}, 1, Vector3{0, 0, 0}});
    world["light"] = new Sphere(Vector3{-5, 3, 0}, Vector3{0, 0, 0}, Vector3{5, 0, 0}, Material{Vector3{0, 0, 0}, 0, Vector3{255, 255, 255}});

    // Initialise the renderer and give it a camera
    Renderer renderer;
    renderer.Init(static_cast<Camera*>(world["camera"]));

    // Main loop
    SDL_Event event;
    SDL_PumpEvents();
    while (running) {
        while (SDL_PollEvent(&event)) {
            EventHandler(event);
            SDL_PumpEvents();
        }

        renderer.Render(world);
    }

    // Exit
    // No exit = no segfault, shrimple (TODO)
    //SDL_Quit();
    return 0;
}

