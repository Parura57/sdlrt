#ifndef __OBJECTS_H
#define __OBJECTS_H

#include"math.h"

#include<string>
#include<unordered_map>

struct Material {
    Vector3 color;
    double specular;
    Vector3 emission;
};


class Object3D {
private:
    Vector3 position;
    Vector3 rotation;
    Vector3 scale;
    Material material;

public:
    Object3D(Vector3 position, Vector3 rotation, Vector3 scale, Material material);

    Vector3 GetPosition();
    Vector3 GetRotation();
    Vector3 GetScale();
    Material GetMaterial();

    void SetPosition(Vector3 p);
    void SetRotation(Vector3 r);
    void SetScale(Vector3 s);
};


// Let's say x scale represents the radius
class Sphere : public Object3D {
public:
    Sphere(Vector3 position, Vector3 rotation, Vector3 scale, Material material);
};


class Camera : public Object3D {
private:
    double fov;
public:
    Camera(Vector3 position, Vector3 rotation, Vector3 scale, double fov);

    double GetFov();
};


struct HitInfo {
    Vector3 coordinates;
    Object3D* object;
    Vector3 color;
};

class Ray {
private:
    Vector3 origin;
    Vector3 direction;

public:
    Ray(Vector3 origin, Vector3 direction);
    HitInfo Cast(int bounces, std::unordered_map<std::string, Object3D*> world);
};

#endif
