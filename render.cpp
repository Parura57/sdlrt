#include"render.h"

#include"settings.h"

#include<cmath>
#include<stdio.h>

void Renderer::Init(Camera* cam) {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) { printf("SDL could not initialize, Error: %s \n", SDL_GetError()); throw; }
    w = SDL_CreateWindow("Raytracer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenW, screenH, SDL_WINDOW_SHOWN);
    if (w == NULL) { printf("Window could not be created, Error: %s \n", SDL_GetError()); throw; }
    windowSurface = SDL_GetWindowSurface(w);

    camera = cam;
}

void Renderer::Render(std::unordered_map<std::string, Object3D*> world) {
    const double fovR = camera->GetFov()/360*6.283185307179586;
    Vector3 colors;
    HitInfo hit;

    SDL_Surface *canvas = SDL_CreateRGBSurfaceWithFormat(0, screenW, screenH, 32, SDL_PIXELFORMAT_RGBA8888);
    Uint32 *buffer = (Uint32*) canvas->pixels;
    Uint32 color;
    SDL_LockSurface(canvas);
    for (int y = 0; y < screenH; y++) {
        for (int x = 0; x < screenW; x++) {
            // First is at -fov/2, last at fov/2, regardless of aspect ratio
            // tan-1(z/x) = -fov/screenW*pixel_x
            // looking along the x axis, with y up and z right
            // taking x=1
            // z = tan(-fov/screenW*pixel_x)
            colors = {0, 0, 0};
            for (int i = 0; i < raysPerPixel; i++) {
                Ray ray(camera->GetPosition(), Vector3 {1, std::tan(static_cast<float>(fovR/screenH/2*y)), static_cast<float>(std::tan(fovR/screenW/2*x))});
                hit = ray.Cast(bounces, world);
                if (hit.object != NULL) colors = colors + Vector3 {hit.color.x, hit.color.y, hit.color.z};
                else {
                    // Set to the same as SkyboxColor, TODO because lazy
                    colors = colors + static_cast<double>(raysPerPixel) * Vector3 {50, 50, 50};
                    break;
                }
                //printf("from %i %i color is %.1f %.1f %.1f\n", x, y, hit.color.x, hit.color.y, hit.color.z);
            }
            colors = (1/static_cast<double>(raysPerPixel))*colors;

            color = SDL_MapRGBA(canvas->format, static_cast<char>(colors.x), static_cast<char>(colors.y), static_cast<char>(colors.z), 255);
            buffer[(screenH-y)*screenW + x] = color;
        }
        printf("column %i\n", y);
    }
    SDL_UnlockSurface(canvas);
    SDL_BlitSurface(canvas, 0, windowSurface, 0);
    SDL_UpdateWindowSurface(w);

    printf("frame\n");
}

