#ifndef __MATH_H
#define __MATH_H
     // ^^ Notice the double underscore

struct Vector3 {
    double x;
    double y;
    double z;

    //Vector3 operator+ (const Vector3& lhs, const Vector3& rhs);
    //Vector3 operator- (const Vector3& obj);
    //Vector3 operator* (const double& lhs, const Vector3& rhs);
    //double operator* (const Vector3& lhs, const Vector3& rhs);
};

Vector3 V_GetVector(Vector3 a, Vector3 b);
Vector3 V_XProduct(Vector3 a, Vector3 b);
double V_Norm(Vector3 v);

    Vector3 operator+ (const Vector3& lhs, const Vector3& rhs);
    Vector3 operator- (const Vector3& obj);
    Vector3 operator* (const double& lhs, const Vector3& rhs);
    bool operator== (const Vector3& lhs, const Vector3& rhs);
    bool operator!= (const Vector3& lhs, const Vector3& rhs);
    double operator* (const Vector3& lhs, const Vector3& rhs);

#endif
