#include"math.h"

// Only for sqrt
#include<cmath>

Vector3 operator+ (const Vector3& lhs, const Vector3& rhs) {
    Vector3 out;
    out.x = lhs.x + rhs.x;
    out.y = lhs.y + rhs.y;
    out.z = lhs.z + rhs.z;
    return out;
}

Vector3 operator- (const Vector3& obj) {
    Vector3 out;
    out.x = -obj.x;
    out.y = -obj.y;
    out.z = -obj.z;
    return out;
}

Vector3 operator* (const double& lhs, const Vector3& rhs) {
    Vector3 out;
    out.x = lhs * rhs.x;
    out.y = lhs * rhs.y;
    out.z = lhs * rhs.z;
    return out;
}

bool operator== (const Vector3& lhs, const Vector3& rhs) {
    return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}

bool operator!= (const Vector3& lhs, const Vector3& rhs) {
    return !(lhs==rhs);
}

double operator* (const Vector3& lhs, const Vector3& rhs) {
    return lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z; 
}

Vector3 V_GetVector(Vector3 a, Vector3 b) {
    Vector3 out;
    out.x = b.x + a.x;
    out.y = b.y + a.y;
    out.z = b.z + a.z;
    return out;
}

Vector3 V_XProduct(Vector3 a, Vector3 b){
    Vector3 out;
    out.x = a.y*b.z - a.z*b.y;
    out.y = a.z*b.x - a.x*b.z;
    out.x = a.x*b.y - a.y*b.x;
    return out;
}

double V_Norm(Vector3 v) {
    return std::sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

